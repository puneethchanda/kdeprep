# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/puneethchanda/projects/tutorial/main.cpp" "/home/puneethchanda/projects/tutorial/build/CMakeFiles/tutorial2.dir/main.cpp.o"
  "/home/puneethchanda/projects/tutorial/mainwindow.cpp" "/home/puneethchanda/projects/tutorial/build/CMakeFiles/tutorial2.dir/mainwindow.cpp.o"
  "/home/puneethchanda/projects/tutorial/build/tutorial2_autogen/mocs_compilation.cpp" "/home/puneethchanda/projects/tutorial/build/CMakeFiles/tutorial2.dir/tutorial2_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "KCOREADDONS_LIB"
  "QT_CORE_LIB"
  "QT_DBUS_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_WIDGETS_LIB"
  "QT_XML_LIB"
  "_GNU_SOURCE"
  "_LARGEFILE64_SOURCE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "tutorial2_autogen/include"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/KF5/KCoreAddons"
  "/usr/include/KF5"
  "/usr/include/KF5/KI18n"
  "/usr/include/KF5/KXmlGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtXml"
  "/usr/include/KF5/KConfigCore"
  "/usr/include/KF5/KConfigWidgets"
  "/usr/include/KF5/KCodecs"
  "/usr/include/KF5/KWidgetsAddons"
  "/usr/include/KF5/KConfigGui"
  "/usr/include/KF5/KAuth"
  "/usr/include/x86_64-linux-gnu/qt5/QtDBus"
  "/usr/include/KF5/KTextWidgets"
  "/usr/include/KF5/SonnetUi"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
