#ifndef MAINWINDOW_H
#define MAINWINDOW_H
 
#include <KF5/KXmlGui/KXmlGuiWindow>

class KTextEdit;
 
class MainWindow : public KXmlGuiWindow
{
  public:
    explicit MainWindow(QWidget *parent = nullptr);
 
  private:
    KTextEdit* textArea;
};
 
#endif
