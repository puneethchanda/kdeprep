#include "mainwindow.h"

#include <KTextEdit>
#include <QApplication>

MainWindow::MainWindow(QWidget *parent) : KXmlGuiWindow(parent)
{
  textArea = new KTextEdit();
  setCentralWidget(textArea);
  setupGUI();
}
